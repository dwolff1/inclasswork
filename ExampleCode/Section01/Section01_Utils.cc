#include "Section01_Utils.h"

/*************************
 *  Function Name: allocateStrMemory
 *  Preconditions: T*, size_t length 
 *  Postconditions: None
 * 
 *  Allocates memory for an array of a given length 
 * ************************/
template<class T>
void allocateMemory(T* arr, const size_t length){
    arr = (T *)malloc(length * sizeof(char));
}

/*************************
 *  Function Name: deallocateStrMemory
 *  Preconditions: char **
 *  Postconditions: None
 * 
 *  Deallocates memory for an array 
 * ************************/
template<class T>
void deallocateMemory(T* arr){
    free(arr);
}

/******************************
 * Function Name: copy
 * Preconditions: InputIterator first, InputIterator last, OutputIterator result
 * Postconditions: OutputIterator
 * Copies the elements in the range [first,last) into the range beginning at result.
 * ****************************/       
template<class InputIterator, class OutputIterator>
OutputIterator copy (InputIterator first, InputIterator last, OutputIterator result)
{
	while (first!=last) {
		*result = *first;
		++result; ++first;
	}
	return result;
}

 /******************************
 * Function Name: find
 * Preconditions: InputIterator first, InputIterator last, const T& 
 * Postconditions: InputIterator
 * 
 * Iterates from the first to the last element. If a value
 * is found, it returns the iterator of that element
 * Otherwise, it will return the last iterator
 * ****************************/
template<class InputIterator, class T>
InputIterator find (InputIterator first, InputIterator last, const T& val)
{
    while (first!=last) {
      if (*first==val) 
        return first;
      ++first;
    }
    return last;
}

/******************************
 * Function Name: swap
 * Preconditions: T2&, T2&
 * Postconditions: none
 * 
 * This swaps the members, and is called
 * by the public void swap(Array<T>& other)
 * ****************************/
template<class T2>
void swap (T2& a, T2& b)
{
	T2 c(a); 
	a = b; 
	b = c;
}

/******************************
 * Function Name: clearSTDCIN
 * Preconditions: none 
 * Postconditions: none
 * 
 * Clears the std::cin buffer in the event 
 * that the user wishes to after an error.
 * ****************************/
void clearSTDCIN(){
	std::cin.clear();
    std::cin.ignore(200, '\n');
}
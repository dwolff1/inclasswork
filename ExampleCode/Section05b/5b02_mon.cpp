/**********************************************
* File: 5b1_mon.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* File for the Brute Force solution to the
* Triple Step problem 
**********************************************/

#include<iostream>
#include "Array.h"

/********************************************
* Function Name  : countways
* Pre-conditions : int n
* Post-conditions: int
*  
* Recursively calls the countways function
********************************************/
int countways(int n);

/********************************************
* Function Name  : countways
* Pre-conditions : int n, Array<int>* memo
* Post-conditions: int
*  
* Recursively calls the countways function
********************************************/
int countways(int n, Array<int>* memo);

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char**argv
* Post-conditions: int
* 
* This is the main driver function
* Assumes argv[1] is a valid integer 
********************************************/
int main(int argc, char**argv){

	std::cout << countways(atoi(argv[1])) << std::endl;

	return 0;
}

int countways(int n){
	
	// Initialize the int array 
	Array<int>* memo = new Array<int>(n+1);
	
	// Fills all values with -1
	for(int iter = 0; iter <= n; iter++){
		(*memo)[iter] = -1;
	}
	return countways(n, memo);
}

int countways(int n, Array<int>* memo){
	if(n < 0)
		return 0;
	else if(n == 0)
		return 1;
	else if((*memo)[n] > -1)
		return (*memo)[n];
	else{
		(*memo)[n] = countways(n-1, memo) + countways(n-2, memo) + countways(n-3, memo);
		return (*memo)[n];
	}
}

